FROM jupyterhub/jupyterhub:0.8.1

LABEL MAINTAINER "benjamin.bertrand@esss.se"

# Install docker in the container
RUN wget https://get.docker.com -q -O /tmp/getdocker \
  && chmod +x /tmp/getdocker \
  && sh /tmp/getdocker \
  && rm -f /tmp/getdocker

# Install dockerspawner and ldapauthenticator
# Use a specific commit for ldapauthenticator (patch needed)
RUN /opt/conda/bin/pip install \
    dockerspawner==0.9.1 \
    git+https://github.com/beenje/ldapauthenticator.git@0c0583e6be83d3cc39fd81e1f67053a3eda2f77b
