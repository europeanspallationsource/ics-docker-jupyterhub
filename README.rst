Dockerfile to build jupyterhub ESS Docker image
===============================================

**DEPRECATED**
Project moved to GitLab: https://gitlab.esss.lu.se/ics-docker/jupyterhub

This Dockerfile creates a Docker_ image based on `jupyterhub/jupyterhub`.

It adds docker engine, dockerspawner and ldapauthenticator.


Usage
-----

After updating this repository, you should tag it using the format "<jupyterhub image tag>-ESSX"::

    $ git tag -a 0.7.2-ESS1

To be able to push the image to Docker Hub, you first have to login::

    $ docker login

You can then run::

    $ make push

This will create and push the image `europeanspallationsource/jupyterhub`.
The image is tagged with both `latest` and the current `git tag`.


.. _Docker: https://www.docker.com
